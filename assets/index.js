class YTLessPlayer {

  player;
  video;
  overlay;
  overlayButton;
  overlayBezel;
  params;

  constructor(player, instances) {

    this.player = player;
    this.instances = instances;
    this.params = new URLSearchParams(window.location.search);

    this.player.innerHTML += this.markup;

    this.video = this.player.querySelector('video');
    this.overlay = this.player.querySelector('.ytless-player--overlay');
    this.overlayButton = this.overlay.querySelector('.ytless-player--button');
    this.overlayBezelPause = this.player.querySelector('.ytp-bezel-pause');
    this.overlayBezelPlay = this.player.querySelector('.ytp-bezel-play');

    this.overlay.style.backgroundImage = this.poster;

    this.player.addEventListener('click', this._click);
  }

  get poster() { return `url(${this.video.poster})` }

  get video_id() { return this.params.get('v') }

  get markup () {

    let html, sources;

    sources = this.instances.map(instance => {

      const sources = `
        /* 720p */
        /* <source src="https://${instance}/latest_version?id=${this.video_id}&itag=22">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=45">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=84">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=95"> */
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=102">

        /* 480p */
        /* <source src="https://${instance}/latest_version?id=${this.video_id}&itag=35">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=44">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=83">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=94"> */
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=101">

        /* 360p */
        /* <source src="https://${instance}/latest_version?id=${this.video_id}&itag=34">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=18">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=82">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=93"> */
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=100">

        /* 270p */
        /* <source src="https://${instance}/latest_version?id=${this.video_id}&itag=6"> */

        /* 240p */
        /* <source src="https://${instance}/latest_version?id=${this.video_id}&itag=5">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=92">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=132"> */

        /* 180p */
        /* <source src="https://${instance}/latest_version?id=${this.video_id}&itag=36"> */

        /* 144p */
        /* <source src="https://${instance}/latest_version?id=${this.video_id}&itag=17"> */

        /* 72p */
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=151">

        /* 1080p */
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=399">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=248">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=137">
        /* <source src="https://${instance}/latest_version?id=${this.video_id}&itag=37">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=46">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=85">
        <source src="https://${instance}/latest_version?id=${this.video_id}&itag=96"> */

        /* 3072p */
        /* <source src="https://${instance}/latest_version?id=${this.video_id}&itag=38"> */
      `;

      return sources
    }).join("");

    html = `
    <video poster="https://i.ytimg.com/vi/${this.video_id}/maxresdefault.jpg">
      ${sources}
    </video>`;

    return html
  }

  _click = () => {

    if (this.video.paused) this.play();
    else this.pause();
  }

  play = () => {

    this.overlay.classList.add('disable');

    this._playBezel();
    this.video.play();
  }

  _playBezel = () => {

    this.overlayBezelPlay.classList.add('enable');
    this.overlayBezelPlay.classList.remove('d-none');
    setTimeout(() => {

      this.overlayBezelPlay.classList.remove('enable');
      this.overlayBezelPlay.classList.add('d-none');
    }, 480);
  }

  pause = () => {

    this._pauseBezel();
    this.video.pause();
  }

  _pauseBezel = () => {

    this.overlayBezelPause.classList.add('enable');
    this.overlayBezelPause.classList.remove('d-none');
    setTimeout(() => {

      this.overlayBezelPause.classList.remove('enable');
      this.overlayBezelPause.classList.add('d-none');
    }, 480)
  }
}
